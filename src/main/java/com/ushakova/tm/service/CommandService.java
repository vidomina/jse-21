package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.ICommandRepository;
import com.ushakova.tm.api.service.ICommandService;
import com.ushakova.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public Collection<String> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public Collection<String> getCommandNameList() {
        return commandRepository.getCommandNames();
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

}