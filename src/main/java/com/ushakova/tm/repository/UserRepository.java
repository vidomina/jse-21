package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import com.ushakova.tm.model.User;

import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByEmail(final String email) {
        return entities.stream()
                .filter(u -> u.getEmail().equals(email))
                .findFirst().orElse(null);
    }

    @Override
    public User findByLogin(final String login) {
        return entities.stream()
                .filter(u -> u.getLogin().equals(login))
                .findFirst().orElse(null);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findById(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        return removeByLogin(login);
    }

    @Override
    public User removeUser(final User user) {
        entities.remove(user);
        return user;
    }

}
