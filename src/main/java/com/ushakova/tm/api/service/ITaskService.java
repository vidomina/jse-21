package com.ushakova.tm.api.service;

import com.ushakova.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    Task add(String name, String description, String userId);

}
