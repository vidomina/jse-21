package com.ushakova.tm.api.repository;

import com.ushakova.tm.api.IRepository;
import com.ushakova.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    E add(E entity, String userId);

    void clear(String userId);

    List<E> findAll(String userId);

    List<E> findAll(Comparator<E> comparator, String userId);

    E findById(String id, String userId);

    E findByIndex(Integer index, String userId);

    E findByName(String name, String userId);

    void remove(E entity, String userId);

    E removeById(String id, String userId);

    E removeByIndex(Integer index, String userId);

    E removeByName(String name, String userId);

}
