package com.ushakova.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showArguments();

    void showCommands();

    void showHelp();

    void showSystemInfo();

    void showVersion();

}
