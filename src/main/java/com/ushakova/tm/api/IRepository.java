package com.ushakova.tm.api;


import com.ushakova.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    E add(E entity);

    void clear();

    List<E> findAll();

    E findById(String id);

    void remove(E entity);

    E removeById(String id);

}
