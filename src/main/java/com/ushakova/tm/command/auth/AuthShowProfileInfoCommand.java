package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.model.User;

public class AuthShowProfileInfoCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show user profile.";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("Show Profile:");
        showUserInfo(user);
    }

    @Override
    public String name() {
        return "user-show-profile";
    }

}
