package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandsListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show commands.";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        System.out.println("All commands:");
        for (final AbstractCommand command : commands) {
            final String name = command.name();
            if (name == null) continue;
            System.out.println(name);
        }
    }

    @Override
    public String name() {
        return "commands";
    }

}