package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear all projects.";
    }

    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Task Clear***");
        serviceLocator.getTaskService().clear();
    }

    @Override
    public String name() {
        return "task-clear";
    }

}
